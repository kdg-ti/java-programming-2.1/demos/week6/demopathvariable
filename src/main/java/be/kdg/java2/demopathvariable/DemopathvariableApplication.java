package be.kdg.java2.demopathvariable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemopathvariableApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemopathvariableApplication.class, args);
    }

}
