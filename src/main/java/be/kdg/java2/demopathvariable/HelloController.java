package be.kdg.java2.demopathvariable;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Controller
public class HelloController {
    @GetMapping("/hello/{name}")
    public String sayHello(@PathVariable Optional<String> name, Model model){
        String theName = name.orElse("John Doe");
        model.addAttribute("greeting", "Hello " + theName);
        return "hello";
    }
}
